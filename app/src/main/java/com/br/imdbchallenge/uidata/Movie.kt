package com.br.imdbchallenge.uidata

import java.io.Serializable
import java.math.BigDecimal
import java.util.*

class Movie : Serializable {

    var id: Long = 0

    var title: String = ""

    var image: String = ""

    var backgroundImage: String = ""

    var description: String = ""

    var release: Date? = null

    var duration: Int = 0

    var rating: BigDecimal = BigDecimal.ZERO
}