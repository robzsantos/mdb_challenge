package com.br.imdbchallenge.uidata

import java.io.Serializable

class Genre : Serializable {

    var id: Long = 0

    var genre: String = ""

    var totalMoviePages: Int = 0

    var currentMoviePage: Int = 1

    var movies: ArrayList<Movie> = arrayListOf()

}