package com.br.imdbchallenge.catalog.list

import com.br.imdbchallenge.Configuration
import com.br.imdbchallenge.uidata.Catalog
import com.br.imdbchallenge.uidata.Genre
import com.br.imdbchallenge.uidata.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-09.
 */
class MovieListPresenter @Inject constructor() {

    @Inject
    internal lateinit var interactor: MovieListInteractor

    private var view: MovieListView? = null

    private var disposable: Disposable? = null

    private var counterGenreList = 0

    private val catalog = Catalog()

    fun attachView(view: MovieListView) {
        this.view = view
    }

    fun detachView() {
        this.view = null

        disposable?.dispose()

        disposable = null
    }

    fun getConfiguration() {
        view?.startLoading()

        disposable = interactor.getImagesConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                Configuration.apply {
                    this.uriImages = it.images.url
                    this.smallImgSize = it.images.size[3]
                    this.originalImgSize = it.images.size.last()
                }

                getGenderList()

            }) { throwable ->

                view?.endLoading()

                val message: String? = throwable.message

                view?.showError(message)
            }
    }

    private fun getGenderList() {
        var genreListSize = 0
        counterGenreList = 0

        disposable = interactor.getGenreList().toObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { genreListSize = it.size }
            .doOnError { throwable ->

                view?.endLoading()

                view?.showError(throwable.message)
            }
            .flatMapIterable { genreObservable -> genreObservable }
            .subscribe {

                val genre = Genre().apply {
                    this.genre = it.name ?: ""
                    this.id = it.id ?: 0
                }

                getMovieList(genre = genre, genreListSize = genreListSize, update = false)
            }
    }

    fun getMovieList(genre: Genre, update: Boolean, genreListSize: Int = 1) {

        disposable = interactor.getMovieList(page = genre.currentMoviePage, genre = genre.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ movies ->
                movies.results.map { movie ->
                    val movieObj = Movie().apply {
                        this.id = movie?.id?:0
                        this.image = Configuration.uriImages + Configuration.smallImgSize + movie?.posterEndPoint
                        this.title = movie?.title?:""
                    }
                    genre.movies.add(movieObj)
                }
                genre.currentMoviePage++
                if (update) {
                    view?.updateGenreList(genre, movies.results.size)
                } else {
                    counterGenreList++
                    genre.totalMoviePages = movies.totalPages?:0
                    if (movies.results.isNotEmpty())
                        catalog.movieCategories.add(genre)
                    if (genreListSize == counterGenreList) {
                        view?.endLoading()
                        view?.showCatalog(catalog)
                    }
                }
            })
            { throwable ->
                view?.endLoading()
                counterGenreList++
                val message: String? = if (!throwable.message.isNullOrEmpty())
                    throwable.message
                else throwable.cause?.message

                view?.showError(message)
                if(!update && counterGenreList == genreListSize && catalog.movieCategories.isNotEmpty())
                    view?.showCatalog(catalog)
            }
    }
}