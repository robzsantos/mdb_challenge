package com.br.imdbchallenge.catalog.list

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.br.imdbchallenge.R
import com.br.imdbchallenge.uidata.Genre
import com.br.imdbchallenge.uidata.Movie
import com.br.imdbchallenge.utils.BinderEpoxyModel
import kotlinx.android.synthetic.main.item_movie_list.view.*

/**
 * Created by Robson on 2019-07-12.
 */
class MovieListModel(
    private val genre: Genre, private val listener: (id: Long) -> Unit,
    private val loadMovies: (genre: Genre) -> Unit
) : BinderEpoxyModel<ConstraintLayout>() {

    var adapterCategory: CategoryMovieAdapter? = null

    override fun getDefaultLayout() = R.layout.item_movie_list

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        view.textview_genre.text = genre.genre

        view.recyclerview_movie_list.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)

        adapterCategory = CategoryMovieAdapter(listener)

        view.recyclerview_movie_list.adapter = adapterCategory

        view.recyclerview_movie_list.onNextLoad { _, _ ->
            if (genre.currentMoviePage <= genre.totalMoviePages)
                loadMovies(genre)
            else
                adapterCategory?.removeLoading()
        }

        adapterCategory?.addMovies(genre.movies)
    }

    fun update(movies: List<Movie>) = adapterCategory?.addMovies(movies = movies)
}