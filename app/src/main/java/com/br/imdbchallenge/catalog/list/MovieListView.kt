package com.br.imdbchallenge.catalog.list

import com.br.imdbchallenge.uidata.Catalog
import com.br.imdbchallenge.uidata.Genre

/**
 * Created by Robson on 2019-07-09.
 */
interface MovieListView {

    fun showError(message: String?)

    fun startLoading()

    fun endLoading()

    fun showCatalog(catalog: Catalog)

    fun updateGenreList(genre: Genre, qtyNewMovies: Int)
}