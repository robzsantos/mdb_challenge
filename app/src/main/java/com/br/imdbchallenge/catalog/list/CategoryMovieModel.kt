package com.br.imdbchallenge.catalog.list

import androidx.constraintlayout.widget.ConstraintLayout
import com.br.imdbchallenge.R
import com.br.imdbchallenge.uidata.Movie
import com.br.imdbchallenge.utils.BinderEpoxyModel
import com.br.imdbchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.item_movie.view.*

/**
 * Created by Robson on 2019-07-12.
 */
class CategoryMovieModel(private val movie: Movie, private val listener: (movieId: Long) -> Unit) :
    BinderEpoxyModel<ConstraintLayout>() {

    override fun getDefaultLayout() = R.layout.item_movie

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        GlideApp.with(view.context)
            .load(movie.image)
            .placeholder(R.drawable.image_placeholder)
            .into(view.imageViewPoster)

        view.setOnClickListener { listener(movie.id) }

    }
}