package com.br.imdbchallenge.catalog.list

import com.br.imdbchallenge.webservice.response.ConfigResponse
import com.br.imdbchallenge.webservice.response.GenreResponse
import com.br.imdbchallenge.webservice.response.MovieListResponse
import com.br.imdbchallenge.webservice.ImdbApi
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-09.
 */
class MovieListInteractor @Inject constructor() {

    @Inject
    lateinit var api: ImdbApi

    fun getGenreList(): Single<List<GenreResponse>> = api.getGenres().map { it.genres.toList() }

    fun getMovieList(page: Int, genre:Long): Single<MovieListResponse> = api.getMoviesByGenre(page, genre)

    fun getImagesConfiguration(): Single<ConfigResponse> = api.getImageConfiguration()

}