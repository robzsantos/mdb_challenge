package com.br.imdbchallenge.catalog.list

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import com.br.imdbchallenge.di.AppComponent
import com.br.imdbchallenge.MdbChallengeApplication
import com.br.imdbchallenge.R
import com.br.imdbchallenge.catalog.detail.MovieDetailsActivity
import com.br.imdbchallenge.uidata.Catalog
import com.br.imdbchallenge.uidata.Genre
import com.br.imdbchallenge.extensions.error
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading.*
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-09
 */
class MovieListActivity : AppCompatActivity(), MovieListView {

    @Inject
    lateinit var presenter: MovieListPresenter

    private val appComponent: AppComponent
        get() {
            val application = application as MdbChallengeApplication

            return application.appComponent ?: throw IllegalStateException()
        }

    private var adapter: MovieListAdapter? = null

    private var catalog: Catalog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        appComponent.inject(this)

        setupRecyclerView()

        presenter.attachView(this)

        catalog = if (savedInstanceState != null) savedInstanceState.getSerializable(CATALOG) as Catalog? else null

        if (catalog == null)
            presenter.getConfiguration()
        else
            showCatalog(catalog!!)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (catalog != null) {
            outState?.putSerializable(CATALOG, catalog)
        }
        super.onSaveInstanceState(outState)

    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun showError(message: String?) {
        error(message)
    }

    override fun startLoading() {
        loading_frame.visibility = View.VISIBLE
    }

    override fun endLoading() {
        loading_frame.visibility = View.GONE
    }

    override fun showCatalog(catalog: Catalog) {
        this.catalog = catalog
        adapter?.addMovies(catalog.movieCategories)
    }

    override fun updateGenreList(genre: Genre, qtyNewMovies: Int) {

        val index = catalog?.movieCategories?.indexOf(genre)
        val newMoviesToAdd = catalog?.movieCategories?.get(index!!)?.movies?.takeLast(qtyNewMovies)

        index?.let {
            adapter?.updateGenreModel(index, newMoviesToAdd ?: listOf())
        }
    }

    private fun setupRecyclerView() {

        recyclerview_genre_list.layoutManager = LinearLayoutManager(this)

        adapter = MovieListAdapter({
            val intent = MovieDetailsActivity.newInstance(this, it)

            startActivity(intent)
        }, {
            presenter.getMovieList(genre = it, update = true)
        })

        recyclerview_genre_list.adapter = adapter
    }

    companion object {
        private const val CATALOG = "catalog"
    }

}