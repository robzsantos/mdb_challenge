package com.br.imdbchallenge.catalog.detail

import com.br.imdbchallenge.uidata.Movie

/**
 * Created by Robson on 2019-07-14.
 */
interface MovieDetailsView {

    fun showError(message: String?)

    fun startLoading()

    fun endLoading()

    fun showMovieInfo(movie: Movie)

}