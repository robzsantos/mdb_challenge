package com.br.imdbchallenge.catalog.list

import com.br.imdbchallenge.uidata.Movie
import com.br.imdbchallenge.utils.BaseEpoxyAdapter
import com.br.imdbchallenge.utils.listview.LoadingModel

/**
 * Created by Robson on 2019-07-12.
 */
class CategoryMovieAdapter(private val listener: (id: Long) -> Unit) : BaseEpoxyAdapter() {

    private val loadingModel = LoadingModel()

    fun addMovies(movies: List<Movie>) {

        removeModel(loadingModel)

        addModels(
            movies.map { CategoryMovieModel(it, listener) }.toList()
        )

        addModel(loadingModel)
    }

    fun removeLoading() {
        removeModel(loadingModel)
    }

}