package com.br.imdbchallenge.catalog.list

import com.br.imdbchallenge.uidata.Genre
import com.br.imdbchallenge.uidata.Movie
import com.br.imdbchallenge.utils.BaseEpoxyAdapter

/**
 * Created by Robson on 2019-07-12.
 */
class MovieListAdapter(
    private val listener: (movieId: Long) -> Unit,
    private val loadMovies: (genre: Genre) -> Unit
) : BaseEpoxyAdapter() {

    fun addMovies(catalog: ArrayList<Genre>) {

        addModels(
            catalog.map { MovieListModel(it, listener, loadMovies) }.toList()
        )
    }

    fun updateGenreModel(index: Int, movies: List<Movie>) = (models[index] as MovieListModel).update(movies)

}