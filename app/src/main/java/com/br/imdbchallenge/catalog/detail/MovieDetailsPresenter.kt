package com.br.imdbchallenge.catalog.detail

import com.br.imdbchallenge.Configuration
import com.br.imdbchallenge.uidata.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-14.
 */
class MovieDetailsPresenter @Inject constructor() {

    @Inject
    internal lateinit var interactor: MovieDetailsInteractor

    private var view: MovieDetailsView? = null

    private var disposable: Disposable? = null

    fun attachView(view: MovieDetailsView) {
        this.view = view
    }

    fun detachView() {
        this.view = null

        disposable?.dispose()

        disposable = null
    }

    fun getMovieInfos(id: Long) {
        view?.startLoading()

        disposable = interactor.getMovieDetails(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val movie = Movie().apply {
                    this.id = it.id?:0
                    this.image = Configuration.uriImages + Configuration.smallImgSize + it.posterEndPoint
                    this.backgroundImage =
                        Configuration.uriImages + Configuration.originalImgSize + it.posterBackgroundEndPoint
                    this.title = it.title?:""
                    this.description = it.description?:""
                    this.duration = it.duration?:0
                    this.release = it.release
                    this.rating = it.rating?: BigDecimal.ZERO
                }

                view?.showMovieInfo(movie)

                view?.endLoading()

            }) { throwable ->

                view?.endLoading()

                val message: String? = throwable.message

                view?.showError(message)
            }
    }
}