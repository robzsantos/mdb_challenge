package com.br.imdbchallenge.catalog.detail

import com.br.imdbchallenge.webservice.response.MovieResponse
import com.br.imdbchallenge.webservice.ImdbApi
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-14.
 */
class MovieDetailsInteractor @Inject constructor() {

    @Inject
    lateinit var api: ImdbApi

    fun getMovieDetails(id: Long): Single<MovieResponse> = api.getMovie(id)

}