package com.br.imdbchallenge.catalog.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import butterknife.ButterKnife
import com.br.imdbchallenge.MdbChallengeApplication
import com.br.imdbchallenge.R
import com.br.imdbchallenge.di.AppComponent
import com.br.imdbchallenge.extensions.error
import com.br.imdbchallenge.uidata.Movie
import com.br.imdbchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.activity_movie_details.*
import kotlinx.android.synthetic.main.loading.*
import java.util.*
import javax.inject.Inject

/**
 * Created by Robson on 2019-07-14.
 */
class MovieDetailsActivity : AppCompatActivity(), MovieDetailsView {

    @Inject
    lateinit var presenter: MovieDetailsPresenter

    //TODO::create BaseActivity and put this property
    private val appComponent: AppComponent
        get() {
            val application = application as MdbChallengeApplication

            return application.appComponent ?: throw IllegalStateException()
        }

    private var movie: Movie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movie_details)

        ButterKnife.bind(this)

        appComponent.inject(this)

        presenter.attachView(this)

        setupToolbar()

        movie =
            if (savedInstanceState != null) savedInstanceState.getSerializable(MOVIE) as Movie? else null

        if (movie == null)
            presenter.getMovieInfos(with(intent) { getLongExtra(EXTRA_MOVIE_ID, 0) })
        else
            showMovieInfo(movie!!)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (movie != null) {
            outState?.putSerializable(MOVIE, movie)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun showError(message: String?) {
        error(message)
    }

    override fun startLoading() {
        loading_frame.visibility = View.VISIBLE
    }

    override fun endLoading() {
        loading_frame.visibility = View.GONE
    }

    override fun showMovieInfo(movie: Movie) {
        this.movie = movie

        supportActionBar?.title = movie.title

        GlideApp.with(this)
            .load(movie.backgroundImage)
            .placeholder(R.drawable.image_placeholder)
            .into(imageview_poster_background)

        GlideApp.with(this)
            .load(movie.image)
            .placeholder(R.drawable.image_placeholder)
            .into(imageview_poster_movie)

        textview_movie_title.text = movie.title

        movie.release?.let {
            val calendar = Calendar.getInstance()

            calendar.time = movie.release

            textview_release_date.text =
                if (movie.duration == 0)
                    calendar.get(Calendar.YEAR).toString()
                else
                    getString(
                        R.string.movie_details_release_date_duration,
                        calendar.get(Calendar.YEAR),
                        movie.duration
                    )
        }
        textview_rating.text =
            if (movie.rating.toInt() == 0)
                getString(R.string.movie_details_no_rating)
            else
                getString(R.string.movie_details_rating, movie.rating)
        textview_movie_description.text = movie.description
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        if (toolbar != null) {
            setSupportActionBar(toolbar)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)

        supportActionBar?.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    companion object {

        private const val MOVIE = "movie"

        private const val EXTRA_MOVIE_ID = "extra_mode_id"

        fun newInstance(context: Context, movieId: Long): Intent =
            Intent(context, MovieDetailsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
                putExtras(Bundle().apply {
                    this.putLong(EXTRA_MOVIE_ID, movieId)
                })
            }
    }
}