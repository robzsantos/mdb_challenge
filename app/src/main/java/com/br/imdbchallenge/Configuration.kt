package com.br.imdbchallenge

/**
 * Created by Robson on 2019-07-15
 */
object Configuration {

    var uriImages: String = ""

    var smallImgSize: String = ""

    var originalImgSize: String = ""

}