package com.br.imdbchallenge.webservice.response

data class ConfigResponse (val images: ImageResponse)