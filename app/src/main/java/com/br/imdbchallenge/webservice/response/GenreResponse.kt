package com.br.imdbchallenge.webservice.response

/**
 * Created by Robson on 2019-07-09.
 */
data class GenreResponse(val id: Long? = 0, val name: String? = null)