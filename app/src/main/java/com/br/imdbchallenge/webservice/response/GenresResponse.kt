package com.br.imdbchallenge.webservice.response

/**
 * Created by Robson on 2019-07-09.
 */
data class GenresResponse(val genres: List<GenreResponse> = emptyList())