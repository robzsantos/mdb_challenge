package com.br.imdbchallenge.webservice.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Robson on 2019-07-09.
 */
data class MovieListResponse(

    @SerializedName("total_pages")
    val totalPages: Int?,

    val results: List<MovieResponse>
)