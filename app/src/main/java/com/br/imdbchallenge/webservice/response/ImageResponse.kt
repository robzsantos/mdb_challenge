package com.br.imdbchallenge.webservice.response

import com.google.gson.annotations.SerializedName

data class ImageResponse(

    @SerializedName("secure_base_url")
    val url:String,

    @SerializedName("poster_sizes")
    val size:List<String>
)