package com.br.imdbchallenge.webservice

import com.google.gson.*
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ismael on 3/27/17.
 */
class DateAdapter : JsonDeserializer<Date> {

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ROOT)

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement?, typeOfT: Type, context: JsonDeserializationContext): Date? {
        if (json == null || json === JsonNull.INSTANCE) {
            return null
        }

        return try {
            val date = json.asString
            if(date.isEmpty())
                null
            else
                dateFormat.parse(date)
        } catch (e: ParseException) {
            throw JsonParseException(e)
        }

    }

}
