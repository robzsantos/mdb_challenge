package com.br.imdbchallenge.webservice

import com.br.imdbchallenge.BuildConfig
import com.br.imdbchallenge.webservice.response.ConfigResponse
import com.br.imdbchallenge.webservice.response.GenresResponse
import com.br.imdbchallenge.webservice.response.MovieListResponse
import com.br.imdbchallenge.webservice.response.MovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Robson on 2019-07-09.
 */
interface ImdbApi {

    @GET("genre/movie/list?api_key=" + BuildConfig.MDB_API_KEY)
    fun getGenres(): Single<GenresResponse>

    @GET("discover/movie?api_key=" + BuildConfig.MDB_API_KEY)
    fun getMoviesByGenre(@Query("page") page: Int, @Query("with_genres") genre: Long): Single<MovieListResponse>

    @GET("configuration?api_key=" + BuildConfig.MDB_API_KEY)
    fun getImageConfiguration(): Single<ConfigResponse>

    @GET("movie/{movie_id}?api_key=" + BuildConfig.MDB_API_KEY)
    fun getMovie(@Path("movie_id") movieId: Long): Single<MovieResponse>

}