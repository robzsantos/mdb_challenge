package com.br.imdbchallenge.webservice.response

import com.br.imdbchallenge.webservice.DateAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.*

data class MovieResponse(

    val id: Long?,

    val title: String?,

    @SerializedName("poster_path")
    val posterEndPoint: String?,

    @SerializedName("backdrop_path")
    val posterBackgroundEndPoint: String?,

    @SerializedName("overview")
    val description: String?,

    @SerializedName("release_date")
    @JsonAdapter(DateAdapter::class)
    val release: Date?,

    @SerializedName("runtime")
    val duration: Int?,

    @SerializedName("vote_average")
    val rating: BigDecimal?
)