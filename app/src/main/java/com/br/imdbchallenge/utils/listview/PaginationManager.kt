package com.br.imdbchallenge.utils.listview

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

/**
 * @link https://gist.github.com/nesquena/d09dc68ff07e845cc622
 * Created by Robz on 25/03/2018.
 */
class PaginationManager : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5

    private var currentPage = 0

    private var previousTotalItemCount = 0

    private var loading = true

    private val startingPageIndex = 0

    private var layoutManager: RecyclerView.LayoutManager? = null

    private var callback: Callback? = null

    fun setupWithRecyclerView(recyclerView: RecyclerView, callback: Callback?) {
        if (callback == null) {
            throw IllegalArgumentException("callback can't be null")
        }

        this.callback = callback

        val layoutManager = recyclerView.layoutManager

        if (layoutManager is GridLayoutManager) {
            this.layoutManager = layoutManager

            visibleThreshold *= layoutManager.spanCount
        } else if (layoutManager is StaggeredGridLayoutManager) {
            this.layoutManager = layoutManager

            visibleThreshold *= layoutManager.spanCount
        } else if (layoutManager is LinearLayoutManager) {
            this.layoutManager = recyclerView.layoutManager
        } else {
            throw IllegalArgumentException("invalid LayoutManger")
        }

        recyclerView.addOnScrollListener(this)
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        var lastVisibleItemPosition = 0
        val totalItemCount = layoutManager!!.itemCount

        if (layoutManager is StaggeredGridLayoutManager) {
            val lastVisibleItemPositions =
                (layoutManager as StaggeredGridLayoutManager).findLastVisibleItemPositions(null)
            // get maximum element within the list
            lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions)
        } else if (layoutManager is GridLayoutManager) {
            lastVisibleItemPosition = (layoutManager as GridLayoutManager).findLastVisibleItemPosition()
        } else if (layoutManager is LinearLayoutManager) {
            lastVisibleItemPosition = (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        }

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }
        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
            currentPage++
            callback?.onLoadMore(currentPage, totalItemCount, recyclerView)
            loading = true
        }
    }

    private fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i]
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }

    // Call this method whenever performing new searches
    fun resetState() {
        this.currentPage = this.startingPageIndex

        this.previousTotalItemCount = 0

        this.loading = true
    }

    interface Callback {

        fun onLoadMore(currentPage: Int, totalItemCount: Int, recyclerView: RecyclerView)

    }

}