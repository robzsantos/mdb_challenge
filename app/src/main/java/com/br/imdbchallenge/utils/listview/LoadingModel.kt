package com.br.imdbchallenge.utils.listview

import android.widget.LinearLayout
import com.airbnb.epoxy.EpoxyModel
import com.br.imdbchallenge.R

/**
 * Created by Robz on 25/03/2018.
 */
class LoadingModel : EpoxyModel<LinearLayout>() {

    private var spanSize: Int = 0

    override fun getDefaultLayout() = R.layout.item_loading_model

    fun setSpanSize(spanSize: Int) {
        this.spanSize = spanSize
    }

    override fun getSpanSize(totalSpanCount: Int, position: Int, itemCount: Int) = spanSize
}