package com.br.imdbchallenge.di.modules

import com.br.imdbchallenge.di.qualifiers.Imdb
import com.br.imdbchallenge.webservice.ImdbApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Robson on 2019-07-09.
 */
@Module
class ApisModule {

    @Provides
    @Singleton
    fun providesExchangesRateApi(@Imdb retrofit: Retrofit): ImdbApi =
        retrofit.create(ImdbApi::class.java)

}