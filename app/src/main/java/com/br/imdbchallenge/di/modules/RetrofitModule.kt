package com.br.imdbchallenge.di.modules

import com.br.imdbchallenge.BuildConfig
import com.br.imdbchallenge.di.qualifiers.Default
import com.br.imdbchallenge.di.qualifiers.Imdb
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Robson on 2019-06-26.
 */
@Module
class RetrofitModule {

    private fun basicBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    @Singleton
    @Imdb
    fun providesExchangeRatesRetrofit(@Default client: OkHttpClient, gson: Gson): Retrofit {
        return basicBuilder(gson)
            .baseUrl(BuildConfig.MDB_API)
            .client(client)
            .build()
    }

}