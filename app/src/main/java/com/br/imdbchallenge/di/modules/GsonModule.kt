package com.br.imdbchallenge.di.modules

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Robson on 2019-07-09.
 */
@Module
class GsonModule {

    @Provides
    @Singleton
    fun providesGson(context: Context): Gson = GsonBuilder().create()
}