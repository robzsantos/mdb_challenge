package com.br.imdbchallenge.di

import com.br.imdbchallenge.catalog.detail.MovieDetailsActivity
import com.br.imdbchallenge.catalog.list.MovieListActivity
import com.br.imdbchallenge.di.modules.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Robson on 2019-07-09.
 */
@Component(modules = [AndroidModule::class, RetrofitModule::class, ApisModule::class, OkHttpClientModule::class, GsonModule::class])
@Singleton
interface AppComponent {

    fun inject(mainActivity: MovieListActivity)

    fun inject(movieDetailsActivity: MovieDetailsActivity)

}