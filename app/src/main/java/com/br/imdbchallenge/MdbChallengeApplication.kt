package com.br.imdbchallenge

import android.app.Application
import com.br.imdbchallenge.di.AppComponent
import com.br.imdbchallenge.di.DaggerAppComponent
import com.br.imdbchallenge.di.modules.AndroidModule

/**
 * Created by Robson on 2019-07-09.
 */
class MdbChallengeApplication : Application() {

    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .androidModule(AndroidModule(this))
            .build()

    }
}